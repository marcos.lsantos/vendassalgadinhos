const express = require('express')
const Category = require('../../models/category')
const MSGS = require('../../messages')
const auth = require('../../midleware/auth')
const {check, validationResult} = require('express-validator')
var router = express.Router()


app.get('/', async(req, res, next) => {
    try{
        const category = await Category.find({})
        res.json(category)
    }catch(err){
        res.json(err.message)
        res.status(500).send({"error": MSGS.GENERIC_ERROR})
    }
})

app.post('/',auth,[
    check('name').not().isEmpty(),
    check('icon').not().isEmpty(),
    ], async(req, res, next) =>{
    try{
        const errors = validationResult(req)

        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array() })
        }else{
            let {name, icon} = req.body
            let category = new Category({name, icon})
            await category.save()
            
            if(category.id){
                res.json(category)
            }
        }
    }catch(err){
        console.json(err.message)
        res.status(500).send({"Error": MSGS.GENERIC_ERROR})
    }

})

module.exports = router