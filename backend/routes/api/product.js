const express = require ('express')
const Product = require ('../../models/product')
const router = express.Router()
const auth = require('../../midleware/auth')
const MSGS = require('../../messages')
const { validationResult } = require('express-validator')


//@router /product/
//@desc list all products
//@access authentication
router.get('/', auth, async(req, res, next) => {
    try{
        const products = await Product.find({})
        res.json(products)
    }catch(err){
        console.res(err)
        res.status(500).send({"erro": MSGS.GENERIC_ERROR})
    }
})



//@router /product/
//@desc insert new products
//@access authentication
router.post('/', auth, async (req, res, next) => {
    try{
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(404).json({errors: errors.array()})
        }else{
            let product = new Product(req.body)
            await product.save()
            if(product.id){
                res.json(product)
            }
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"Error": MSGS.GENERIC_ERROR})
    }
})



//@router /product/:id
//@desc modify a product
//@access authentication
router.patch('/:id', auth, async(req, res, next) => {
    try{
        const product = await Product.findOneAndUpdate(req.params.id, {$set: req.body}, {new: true})
        if(product){
            res.json(product)
        }else{
            res.status(404).send({"Error": MSGS.PRODUCT404})
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"Error": MSGS.GENERIC_ERROR})
    }
})



//@router /product/:id
//@desc remove a product
//@access authentication
router.delete('/:id', auth, async(req, res, next) => {
    try{
        const id = req.params.id
        const product = await Product.findOneAndDelete({_id: id})
        if(product){
            res.json(product)
        }else{
            res.status(404).send({"Error": MSGS.PRODUCT404})
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"Error": MSGS.GENERIC_ERROR})
    }
})


module.exports = router