import React from "react"
import history from './config/history'
import {Router, Switch, Route, Redirect} from 'react-router-dom'
import Home from './components/portal/home/home'
import Login from './views/admin/login/login'
import {isAuthenticated} from './config/auth'

//layout
import Layoutadmin from './components/layout/admin/layoutadmin'
import Layoutportal from './components/layout/portal/layoutportal'

//usuarios
import NewUser from './views/admin/user/newuser'
import ListUser from './views/admin/user/listuser'

//Produtos
import NewProducts from './views/admin/products/newproduct'
import ListProducts from './views/admin/products/listadminproducts'
import ListUserProducts from './views/portal/products/listproducts'

//Categorias
import ListCategories from './views/admin/categories/listcategories'
import NewCategory from './views/admin/categories/newcategory'

const AdminRoute = ({ ...rest }) => {
    if (!isAuthenticated()) {
        return <Redirect to='/' />
    }
    return <Route {...rest} />
}


const Routes = ()=>(
    <Router history={history}>
        <Switch>
            

                <Route exact component={Login} path="/login" />
                 
                    <Route path="/admin">       
                        <Layoutadmin>
                            <AdminRoute exact component={NewUser} path='/admin/newuser'/>
                            <AdminRoute exact component={ListUser} path='/admin/usuarios'/>
                            <AdminRoute exact component={NewProducts}  path="/admin/newproducts"/>
                            <AdminRoute exact component={ListProducts} path="/admin/listproducts"/>
                            <AdminRoute exact component={NewCategory}  path="/admin/newcategory"/>
                            <AdminRoute exact component={ListCategories} path="/admin/listcategories"/>
                        </Layoutadmin>
                    </Route>

                    <Route path="/">   
                        <Layoutportal>              
                            <Route exact path="/" component={Home}/>
                            <Route exact component={ListUserProducts}  path="/products"/>
                        </Layoutportal>
                    </Route>
        </Switch>
    </Router>
)

export default Routes