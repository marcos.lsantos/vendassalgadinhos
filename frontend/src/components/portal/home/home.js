import React from 'react'
import styled from 'styled-components'
import { Row, Col, Button } from 'react-bootstrap'
import { Carousel } from 'react-bootstrap'
import imgassados from '../../../img/assados.png'
import imgfritos from '../../../img/fritos.png'
import imgcomidas from '../../../img/comidas.png'

const Principal = () => {

    return (
        <Banner><Carousel>
        <Carousel.Item>
          <img src={imgfritos}         />
          <Carousel.Caption>
            <h3>Fritos</h3>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img  src={imgassados}       />
      
          <Carousel.Caption>
            <h3>Assados</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img src={imgcomidas}          />
      
          <Carousel.Caption>
            <h3>Comidas</h3>
            </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
        </Banner>
    )
}


export default Principal



const Banner = styled.div`
    height: 500px;
    width: 100%;
`