import React, { useState, useEffect } from 'react'
import { list } from '../../../service/product'
import Card from 'react-bootstrap/Card'
import styled from 'styled-components'


const ProductList = (props) => {
    const [product, setProduct] = useState([])


    const getProduct = async () => {
        const produto = await list()
        if (produto) {
            setProduct(produto.data)
        }
        console.log(produto.data)
    }

    const verifyIsEmpty = product.length === 0

    const lista = () => {
        const conteudo = product.map((produtos, index) => (

            <Card key={index} style={{ width: '18rem' }}>
                <Card.Img variant="top" src={produtos.photo}/> 
                <Card.Body>
                    <Card.Title>{produtos.title}</Card.Title>
                    <Card.Text>{produtos.description}</Card.Text>
                </Card.Body>
            </Card>
        )
        )

        return (
            <Table>
                {conteudo}
            </Table>
        )
    }

    useEffect(() => {
        getProduct()
    }, [])

    return (
        <Salgadinhos>
            {lista()}
        </Salgadinhos>
    )

}

export default ProductList

const Salgadinhos = styled.div`
    width: 100%;
`

const Table = styled.div`
    width: 100%;

    /*estilo para os cards*/
    margin-top: 60px;
    display: flex;
    flex-flow: wrap;
`