import React, {useState, useEffect} from 'react'
import {getAll, deletCategory} from '../../../service/category'
import { useHistory} from 'react-router-dom'
import styled from 'styled-components'
import { FcPlus } from 'react-icons/fc'
import {FaTrashAlt} from 'react-icons/fa'



const Listaproduto = (props)=>{
    const [category, setCategory] = useState([])
    const history = useHistory()
    

    const cadastrar = () => {
        history.push('/admin/newcategory')
    }
  
    const getCategories = async()=>{
        const categorias = await getAll()
            if(categorias) {
                setCategory(categorias.data)
            }
    }

    const excluir = async ({ _id, name }) => {
        try {
            if (window.confirm(`Você deseja excluir a categoria ${ name}`)) {
                await deletCategory(_id)
                getCategories()
            }
        } catch (error) {
            console.log(error)
        }
    }

    const verifyIsEmpty = category.length === 0

    const lista = () => {
        const conteudo = category.map((dados, index) => (
            
            <TRTable key={index}>
                <td>{dados.name}</td>
                <td>{dados.photo}</td>
                <td>
                    <span onClick={() => excluir(dados)}>  <FaTrashAlt /> </span>
                </td>
            </TRTable>
            )
        )

        return(
            <Table>
                {!verifyIsEmpty ?(
                    <LayoutTable>
                        <nav>
                            <Titulo> <h1>Categorias</h1></Titulo>
                            <Botao>
                                <button onClick={cadastrar}><FcPlus /> Nova</button>
                            </Botao>
                        </nav>                    
                        <LayoutTable>
                            <Thead>
                                <TRTable>
                                    <td>Nome</td>
                                    <td>Icon</td>
                                </TRTable>
                            </Thead>
                            <tbody>
                                {conteudo}
                            </tbody>
                        </LayoutTable>

                    </LayoutTable>
                ):""
            }
            </Table>
        )
    }

    useEffect(() => {
        getCategories()
    },[])

    return(
        <Categorias>
            {lista()}
        </Categorias>
    )

}

export default Listaproduto

const Categorias = styled.div`
    width: 100%;
`

const LayoutTable = styled.table`
    width: 100%;
`

const Table = styled.div`
    width: 100%;
`

const Thead = styled.thead`
    background-color: grey;
    color: white;
    border: 1px solid black;
    text-align: center;
`

const TRTable = styled.tr`
    text-align: center;
`

const Titulo = styled.div`

`

const Botao = styled.div`
`