import React, { useState} from 'react'
import styled from 'styled-components'
import history from '../../../config/history'
import Form from 'react-bootstrap/Form'
import {registerCategory} from '../../../service/category'
import {Row, Col, Button} from 'react-bootstrap'

const InputCategory = (props) => {
    const [category, setCategory] = useState([])

    const typeReq = (data) => registerCategory(data)

    const guardaCategoria = (attr) => {
        const {value, name, checked} = attr.target
        const isCheck  = name === 'status'

        if(name === 'photo') {
            setCategory({
                ...category,
                'photo': attr.target.files[0]
        })
        }else{
            setCategory({
                ...category,
                [name]: isCheck ? checked : value
            })
        }
        return
    }

    const enviarDados = async (e) => {        
        e.preventDefault()
        
        let data = new FormData()

        Object.keys(category)
            .forEach(key => data.append(key, category[key]))

        const config ={
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }
        typeReq(data, config)
        history.push('/admin/listcategories')
    }




    return (

        <Register>
            <Formulario>
                <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={2}>
                        Insira uma nova categoria de Produto
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="text" name="name" value={category.name} onChange={guardaCategoria} />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalEmail">
                    <input name="photo" type="file" onChange={guardaCategoria} />
                </Form.Group>

                <Form.Group as={Row}>
                    <Butao>
                        <Button type="submit" onClick={(e)=>enviarDados(e)} >Cadastrar</Button>
                    </Butao>
                </Form.Group>
            </Formulario>

        </Register>

    )
}

export default InputCategory


const Register = styled.div`
    padding: 4px;
    margin: 10%;
    display: flex;
    justify-content: center;
`

const Formulario = styled.form`
    border: 1px solid;
    padding: 50px;
    border-radius: 4px;
    background-color: aliceblue;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
`

const Butao = styled.div`
    display: flex;
    justify-content: center;
`