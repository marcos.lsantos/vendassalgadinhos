import React, { useState, useEffect } from 'react'
import { listUser, deletUser } from '../../../service/user'
import history from '../../../config/history'
import { FcPlus } from 'react-icons/fc'
import styled from 'styled-components'
import { FaTrashAlt } from 'react-icons/fa'


const UserList = (props) => {
    const [user, setUser] = useState([])

    const cadastrar = () => {
        history.push('/admin/newuser')
    }


    const excluir = async ({ _id, name }) => {
        try {
            if (window.confirm(`Você deseja excluir o usuario ${name}`)) {
                await deletUser(_id)
                getUser()
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getUser = async () => {
        const user = await listUser()
        if (user) {
            setUser(user.data)
        }
        console.log(user.data)
    }

    const lista = () => {
        const conteudo = user.map((usuarios, index) => (
            
            <TRTable key={index}>
                <td>{usuarios.name}</td>
                <td>{usuarios.email}</td>
                <td>{usuarios.is_admin} </td>
                <td>
                    <span onClick={() => excluir(usuarios)}>  <FaTrashAlt /> </span>
                </td>
            </TRTable>
            
        )
        
        )
        

        return (
            <Table>
                <nav>
                    <Titulo> <h1>Lista de Usuários</h1></Titulo>
                    <Botao>
                        <button onClick={cadastrar}><FcPlus /> Novo</button>
                    </Botao>
                </nav>
                <LayoutTable>
                    <Thead>
                        <TRTable>
                            <td>Nome</td>
                            <td>Email</td>
                            <td>Administrador</td>
                            <td>Procedimentos</td>
                        </TRTable>
                    </Thead>
                    <tbody>
                        {conteudo}
                    </tbody>
                </LayoutTable>
              
            </Table>
        )
    }

    useEffect(() => {
        getUser()
    }, [])

    return (
        <Usuarios>
            {lista()}
        </Usuarios>
    )

}

export default UserList

const Thead = styled.thead`
    background-color: grey;
    color: white;
    border: 1px solid black;
    text-align: center;
`

const Usuarios = styled.div`
    width: 100%
`
const TRTable = styled.tr`
    text-align: center;
`

const Table = styled.div`
    width: 100%;
`

const LayoutTable = styled.table`
    width: 100%;
`

const Titulo = styled.div`

`

const Botao = styled.div`
    dislpay: flex;
    justify-content: center;
`