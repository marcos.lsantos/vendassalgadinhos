import React, { useState} from 'react'
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'
import Form from 'react-bootstrap/Form'
import {createUser} from '../../../service/user'
import {Row, Col, Button} from 'react-bootstrap'

const InputUser = (props) => {
    const [user, setUser] = useState([])    
    const history = useHistory()

    const guardaUser = (usuario) => {
        setUser({
            ...user,
            [usuario.target.name]: usuario.target.value
        })
        return
    }

    const enviarDados = async (e) => {        
        e.preventDefault()
        console.log(user)
        const resultado = await createUser(user)
        alert("Cadastro efetuado")
        history.push('/admin/usuarios')
    }




    return (
        <>
        <Register>
            <Formulario>
                <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={2}>
                        Email
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="email" name="email" value={user.email} onChange={guardaUser} placeholder="Email" />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={2}>
                        Nome
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="text" name="name" value={user.nome} onChange={guardaUser} placeholder="Seu nome aqui" />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalPassword">
                    <Form.Label column sm={2}>
                        Password
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control type="password" name="password" value={user.password} onChange={guardaUser} placeholder="Password" />
                    </Col>
                </Form.Group>
                    <Form.Group as={Row}>
                        <Form.Label as="legend" column sm={2}>
                            Adminnistrador
                        </Form.Label>
                        <Col sm={10}>
                            <Form.Check
                                type="radio"
                                label="Sim"
                                onChange={guardaUser}
                                value={true}
                                name="is_admin"
                                id="formHorizontalRadios1"
                            />
                            <Form.Check
                                type="radio"
                                label="Não"
                                onChange={guardaUser}
                                value={false}
                                name="is_admin"
                                id="formHorizontalRadios2"
                            />
                        </Col>
                    </Form.Group>
                <Butao>
                    <Button type="submit" onClick={(e)=>enviarDados(e)} >Cadastrar</Button>
                </Butao>
            </Formulario>
        </Register>
        
    </>
    )
}

export default InputUser


const Register = styled.div`
    padding: 4px;
    margin: 10%;
    display: flex;
    justify-content: center;
`
const Formulario = styled.form`
    border: 1px solid;
    padding: 50px;
    border-radius: 4px;
    background-color: aliceblue;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
`

const Butao = styled.div`
    display: flex;
    justify-content: center;
`