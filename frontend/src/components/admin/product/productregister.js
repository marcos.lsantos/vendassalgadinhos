import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import Form from 'react-bootstrap/Form'
//import { getAll } from '../../service/category'
import { Row, Col, Button } from 'react-bootstrap'
import { registerProduct } from '../../../service/product'
import history from '../../../config/history'

const InputProduct = (props) => {
    const [product, setProduct] = useState([])
    const [category, setCategory] = useState([])


    const typeReq = (data) => registerProduct(data)

    const guardaProduct = (attr) => {
        const {value, name, checked} = attr.target
        const isCheck = name === "status"
        
        if(name === "photo"){
            setProduct({
                ...product,
                'photo': attr.target.files[0]
        })
    }else{
        setProduct({
            ...product,
            [name]: isCheck ? checked : value
        })
    }
        return
    }

    const enviarDados = async (e) => {
        e.preventDefault()
        let data = new FormData()


        Object.keys(product)
            .forEach(key => data.append(key, product[key]))

        const config = {
            headers: {
                'Content-type': 'multipart/form-data'
            }
        }
        console.log(data)
        typeReq(data, config)
        .then( () => history.push("/admin/listproducts") )
        
    }

   /*  const getCategoria = async () => {
        const categorias = await getAll()
        if (categorias) {
            setCategory(categorias.data)
        }
        console.log(categorias.data)
    } */

    const showCategory = category.map((cat, index) => (
        <option value={cat._id}>{cat.name}</option>

/*         <select value={this.state.value} onChange={this.handleChange}>            <option value="laranja">Laranja</option>
            <option value="limao">Limão</option>
            <option value="coco">Coco</option>
            <option value="manga">Manga</option>
          </select>
 */
    ))



    useEffect(() => {
       // getCategoria()
    }, [])


    return (
        <>
            <Register>
                <Formulario>
                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <input name="photo" type="file" onChange={guardaProduct} />
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            Nome
                    </Form.Label>
                        <Col sm={10}>
                            <Form.Control type="text" name="title" value={product.title} onChange={guardaProduct} placeholder="Nome da arma" />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            Descrição
                    </Form.Label>
                        <Col sm={10}>
                            <Form.Control type="text" name="description" value={product.description} onChange={guardaProduct} placeholder="Descrição " />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formHorizontalEmail">
                        <Form.Label column sm={2}>
                            Valor Estimado
                    </Form.Label>
                        <Col sm={10}>
                            <Form.Control type="number" name="price" value={product.price} onChange={guardaProduct} placeholder="sem casas decimais" />
                        </Col>
                    </Form.Group>

                     <Form.Group as={Row} controlId="formHorizontalEmail">
                    <Form.Label column sm={2}>
                        Categoria
                    </Form.Label>
                    <Col sm={10}>
                        <Form.Control name="category" value={product.category} onChange={guardaProduct} as="select" custom>
                                {showCategory}
                        </Form.Control>
                    </Col>
                </Form.Group>

                    <Form.Group as={Row}>
                        <Col sm={{ span: 10, offset: 2 }}>

                        </Col>
                    </Form.Group>
                    <Butao>
                        <Button type="submit" onClick={(e) => enviarDados(e)} >Cadastrar</Button>
                    </Butao>
                </Formulario>
            </Register>

        </>
    )
}

export default InputProduct


const Register = styled.div`
    padding: 4px;
    margin: 10%;
    display: flex;
    justify-content: center;
`

const Formulario = styled.form`
    border: 1px solid;
    padding: 50px;
    border-radius: 4px;
    background-color: aliceblue;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
`

const Butao = styled.div`
    display: flex;
    justify-content: center;
`