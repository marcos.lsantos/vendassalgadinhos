import React, {useState, useEffect} from 'react'
import {list, deletProduct} from '../../../service/product'
import { useHistory} from 'react-router-dom'
import styled from 'styled-components'
import { FcPlus } from 'react-icons/fc'
import {FaTrashAlt} from 'react-icons/fa'



const Listaproduto = (props)=>{
    const [product, setProduct] = useState([])
    const history = useHistory()
    

    const cadastrar = () => {
        history.push('/admin/newproducts')
    }

    /* const getCategory= async (produtos)=>{
        const id = produtos.category
        const categoryName = await listProduct(id)
        console.log(categoryName)
    } */
  
    const getProduto = async()=>{
        const product = await list()
            if(product) {
                setProduct(product.data)
            }
        console.log(product.data)
    }

    const excluir = async ({ _id, title }) => {
        try {
            if (window.confirm(`Você deseja excluir esse produto ${ title}`)) {
                await deletProduct(_id)
                getProduto()
            }
        } catch (error) {
            console.log(error)
        }
    }

    const verifyIsEmpty = product.length === 0

    const lista = () => {
        const conteudo = product.map((produtos, index) => (
            
            <TRTable key={index}>
                <td>{produtos.title}</td>
                <td></td>
                <td>{produtos.description}</td>
                <td>{produtos.price}</td>
                <td>{produtos.discount_price}</td>
                <td>{produtos._price_percent}</td>
                <td>
                    <span onClick={() => excluir(produtos)}>  <FaTrashAlt /> </span>
                </td>
            </TRTable>
            )
        )

        return(
            <Table>
                {!verifyIsEmpty ?(
                    <LayoutTable>
                        <nav>
                            <Titulo> <h1>Produtos</h1></Titulo>
                            <Botao>
                                <button onClick={cadastrar}><FcPlus /> Nova</button>
                            </Botao>
                        </nav>                    
                        <LayoutTable>
                            <Thead>
                                <TRTable>
                                    <td>Nome</td>
                                    <td>Categoria</td>
                                    <td>Descrição</td>
                                    <td>Preço</td>
                                    <td>Preço com desconto</td>
                                    <td>Porcentagem de Desconto</td>
                                    <td>Procedimentos</td>
                                </TRTable>
                            </Thead>
                            <tbody>
                                {conteudo}
                            </tbody>
                        </LayoutTable>

                    </LayoutTable>
                ):""
            }
            </Table>
        )
    }

    useEffect(() => {
        getProduto()
    },[])

    return(
        <Usuarios>
            {lista()}
        </Usuarios>
    )

}

export default Listaproduto

const Usuarios = styled.div`
    width: 100%;
`

const LayoutTable = styled.table`
    width: 100%;
`

const Table = styled.div`
    width: 100%;
`

const Thead = styled.thead`
    background-color: grey;
    color: white;
    border: 1px solid black;
    text-align: center;
`

const TRTable = styled.tr`
    text-align: center;
`

const Titulo = styled.div`

`

const Botao = styled.div`
`