import React, {useState} from 'react'
import {authentication} from '../../../service/auth'
import {clientHttp} from '../../../config/config'
import {saveToken} from '../../../config/auth'
import {Form, Button} from 'react-bootstrap'
import styled from 'styled-components'
import history from '../../../config/history'


const Login = (props) => {
    const [dados, setLogar] = useState({})

    const logar = (conteudo) => {
        setLogar({
            ...dados,
            [conteudo.target.name]: conteudo.target.value
        })
        return
    }

    const pressEnter = (event) => event.key === 'Enter' ? checkLogin() : null

    const checkLogin = async() =>{
        try{
            const {data: {token, user} } = await authentication(dados)
            clientHttp.defaults.headers['x-auth-token'] = token
            saveToken(token)
            console.log(user)
            if(user.admin === true){
                history.push("/admin/usuarios") 
              
            }else{
                alert("Acesso negado")
                history.push("/")
            }
        }catch(err){
            alert("Usuario ou senha invalidos")
        }
        return
    }

    return (
        <Loginbox>
            <Formulario>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Control type="email" name="email" value={dados.email} onKeyPress={pressEnter} onChange={logar} placeholder="Enter email" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Control type="password" name="password" value={dados.senha} onKeyPress={pressEnter} onChange={logar} placeholder="Password" />
                </Form.Group>
                <Botao>
                    <Button  onClick={checkLogin}>Entrar </Button>
                </Botao>
            </Formulario>

        </Loginbox>

    )
}


export default Login

const Loginbox = styled.div`
    padding: 4px;
    margin: 10%;
    display: flex;
    justify-content: center;
`


const Formulario = styled.form`
    border: 1px solid;
    padding: 50px;
    border-radius: 4px;
    background-color: aliceblue;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
`

const Botao = styled.div`
    margin: 10px;
    display: flex;
`