import React from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

export default (props) => {


    return (
        < Header >
            <Navbar>
                <Logo>
                    <span>Icone</span>
                </Logo>
                <Itens>
                    <NavLink exact={true} to="/">Home</NavLink>
                    <NavLink exact={true} to="/admin/listcategories">Categoria</NavLink>
                    <NavLink exact={true} to='/admin/listproducts'>Produtos</NavLink>
                    <NavLink exact={true} to='/admin/usuarios'>Usuarios</NavLink>
                </Itens>
            </Navbar>
        </Header >
    )
}



const Header = styled.div`
    display: flex;
    background: greenyelow;
    color: white;
    height: 50px;
`

const Logo = styled.div`
    align-itens: center;
    justify-content: center;
`

const Navbar = styled.div`
    width: 100%;
    display: flex;
`

const Itens = styled.div`
    display: flex;
    flex-grow: 1;
    justify-content: flex-end;
    align-itens: center;
    padding: 4px;
    margin: 4px;
`