const {clientHttp} = require('../config/config')

const listProduct = (_id) => clientHttp.get(`/product/${_id}`)

const modifyProduct = (dados) => clientHttp.patch(`/product/${dados.id}`, dados)

const deletProduct = (_id) => clientHttp.delete(`/product/${_id}`)

const list = () => clientHttp.get('/product')

const registerProduct = (dados) => clientHttp.post(`/product`, dados)

export {
    listProduct,
    modifyProduct,
    deletProduct,
    list,
    registerProduct
}