const {clientHttp} = require ('../config/config')

const getCategory = (_id) => clientHttp.get(`/category/${_id}`)

const modifyCategory = (dados) => clientHttp.patch(`/category/${dados._id}`, dados)

const deletCategory = (_id) => clientHttp.delete(`/category/${_id}`)

const getAll = () => clientHttp.get(`/category`)

const registerCategory = (dados) => clientHttp.post(`/category`, dados)

export{
    getCategory,
    modifyCategory,
    deletCategory,
    getAll,
    registerCategory
}