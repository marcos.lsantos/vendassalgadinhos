const {clientHttp} = require("../config/config")

const createUser = (dados) => clientHttp.post(`/user`, dados)

const listUser = () => clientHttp.get(`/user`)

const getUser = (_id) => clientHttp.get(`/user/${_id}`)

const deletUser = (_id) => clientHttp.delete(`/user/${_id}`)

export {
    createUser,
    listUser,
    getUser,
    deletUser
}